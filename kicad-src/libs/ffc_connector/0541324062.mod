PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
MOLEX_0541324062
$EndINDEX
$MODULE MOLEX_0541324062
Po 0 0 0 15 00000000 00000000 ~~
Li MOLEX_0541324062
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -6.36405 -5.3384 1.00064 1.00064 0 0.05 N V 21 "MOLEX_0541324062"
T1 -7.33283 4.36965 1.00106 1.00106 0 0.05 N V 21 "VAL**"
DP 0 0 0 0 6 0 15
Dl -13.05 -1.275
Dl -10.7 -1.275
Dl -10.7 -1.675
Dl -11.95 0.125
Dl -11.95 0.325
Dl -13.05 0.325
DP 0 0 0 0 4 0 23
Dl -13.1546 -1.375
Dl -12.3 -1.375
Dl -12.3 0.425149
Dl -13.1546 0.425149
DP 0 0 0 0 4 0 19
Dl -13.0691 -1.275
Dl -12.25 -1.275
Dl -12.25 0.325475
Dl -13.0691 0.325475
DP 0 0 0 0 4 0 15
Dl 12.1709 -1.275
Dl 13.05 -1.275
Dl 13.05 0.325558
Dl 12.1709 0.325558
DP 0 0 0 0 4 0 23
Dl 12.1573 -1.375
Dl 13.15 -1.375
Dl 13.15 0.425254
Dl 12.1573 0.425254
DP 0 0 0 0 4 0 19
Dl 12.1168 -1.275
Dl 13.05 -1.275
Dl 13.05 0.325452
Dl 12.1168 0.325452
DP 0 0 0 0 4 0 24
Dl -10.6677 -1.525
Dl -10.1 -1.525
Dl -10.1 0.175291
Dl -10.6677 0.175291
DP 0 0 0 0 4 0 24
Dl -10.6506 -1.525
Dl -10.1 -1.525
Dl -10.1 0.175009
Dl -10.6506 0.175009
DP 0 0 0 0 4 0 24
Dl 10.1004 -1.525
Dl 10.65 -1.525
Dl 10.65 0.175008
Dl 10.1004 0.175008
DP 0 0 0 0 4 0 24
Dl 10.1147 -1.525
Dl 10.65 -1.525
Dl 10.65 0.175256
Dl 10.1147 0.175256
DS -12.15 -3.275 -10.3 -3.275 0.2 21
DS 10.3 -3.275 12.15 -3.275 0.2 21
DS 12.15 -3.275 12.15 -2.45 0.2 21
DS 12.15 0.75 12.15 1.425 0.2 21
DS 12.15 1.425 -12.15 1.425 0.2 21
DS -12.15 1.425 -12.15 0.75 0.2 21
DS -12.15 -2.45 -12.15 -3.275 0.2 21
DS -12.75 1.425 -12.15 1.425 0.2 21
DS 12.15 1.425 12.75 1.425 0.2 21
DS 12.75 1.425 12.75 3.275 0.2 21
DS 12.75 3.275 -12.75 3.275 0.2 21
DS -12.75 3.275 -12.75 1.425 0.2 21
DC -9.76 -4.65 -9.63958 -4.65 0.2 21
DC -9.74 -4.62 -9.69758 -4.62 0.2 21
DS -10.25 -4.45 10.25 -4.45 0.05 26
DS 10.25 -4.45 10.25 -3.55 0.05 26
DS 10.25 -3.55 12.5 -3.55 0.05 26
DS 12.5 -3.55 12.5 -1.55 0.05 26
DS 12.5 -1.55 13.3 -1.55 0.05 26
DS 13.3 -1.55 13.3 3.55 0.05 26
DS 13.3 3.55 -13.3 3.55 0.05 26
DS -13.3 3.55 -13.3 -1.55 0.05 26
DS -13.3 -1.55 -12.5 -1.55 0.05 26
DS -12.5 -1.55 -12.5 -3.55 0.05 26
DS -12.5 -3.55 -10.25 -3.55 0.05 26
DS -10.25 -3.55 -10.25 -4.45 0.05 26
$PAD
Sh "1" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.75 -3.575
$EndPAD
$PAD
Sh "2" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -9.25 -3.575
$EndPAD
$PAD
Sh "3" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -8.75 -3.575
$EndPAD
$PAD
Sh "4" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -8.25 -3.575
$EndPAD
$PAD
Sh "5" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -7.75 -3.575
$EndPAD
$PAD
Sh "6" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -7.25 -3.575
$EndPAD
$PAD
Sh "7" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.75 -3.575
$EndPAD
$PAD
Sh "8" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.25 -3.575
$EndPAD
$PAD
Sh "9" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.75 -3.575
$EndPAD
$PAD
Sh "10" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.25 -3.575
$EndPAD
$PAD
Sh "11" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.75 -3.575
$EndPAD
$PAD
Sh "12" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -4.25 -3.575
$EndPAD
$PAD
Sh "13" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.75 -3.575
$EndPAD
$PAD
Sh "14" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.25 -3.575
$EndPAD
$PAD
Sh "15" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.75 -3.575
$EndPAD
$PAD
Sh "16" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.25 -3.575
$EndPAD
$PAD
Sh "17" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.75 -3.575
$EndPAD
$PAD
Sh "18" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.25 -3.575
$EndPAD
$PAD
Sh "19" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.75 -3.575
$EndPAD
$PAD
Sh "20" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.25 -3.575
$EndPAD
$PAD
Sh "21" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.25 -3.575
$EndPAD
$PAD
Sh "22" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.75 -3.575
$EndPAD
$PAD
Sh "23" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.25 -3.575
$EndPAD
$PAD
Sh "24" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.75 -3.575
$EndPAD
$PAD
Sh "25" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.25 -3.575
$EndPAD
$PAD
Sh "26" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.75 -3.575
$EndPAD
$PAD
Sh "27" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.25 -3.575
$EndPAD
$PAD
Sh "28" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.75 -3.575
$EndPAD
$PAD
Sh "29" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.25 -3.575
$EndPAD
$PAD
Sh "30" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 4.75 -3.575
$EndPAD
$PAD
Sh "31" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.25 -3.575
$EndPAD
$PAD
Sh "32" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.75 -3.575
$EndPAD
$PAD
Sh "33" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.25 -3.575
$EndPAD
$PAD
Sh "34" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.75 -3.575
$EndPAD
$PAD
Sh "35" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 7.25 -3.575
$EndPAD
$PAD
Sh "36" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 7.75 -3.575
$EndPAD
$PAD
Sh "37" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 8.25 -3.575
$EndPAD
$PAD
Sh "38" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 8.75 -3.575
$EndPAD
$PAD
Sh "39" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.25 -3.575
$EndPAD
$PAD
Sh "40" R 0.3 1.2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 9.75 -3.575
$EndPAD
$PAD
Sh "41" R 1.6 2.4 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -11.45 -0.875
$EndPAD
$PAD
Sh "44" R 1.6 2.4 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 11.45 -0.875
$EndPAD
$EndMODULE MOLEX_0541324062
