EESchema Schematic File Version 4
LIBS:futokoroshi-cache
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 4
Title "Futokoroshi Pocket Computer - mother board"
Date "2019-09-16"
Rev "endless-beta"
Comp "tokinowari.net"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1250 1950 3000 2000
U 5D7F6233
F0 "Power Source" 50
F1 "power_source.sch" 50
$EndSheet
$Sheet
S 6700 1850 2700 7550
U 5D814579
F0 "RaspberryPi IO Pins" 50
F1 "raspi_io-pins.sch" 50
$EndSheet
$Sheet
S 12400 7700 2850 1750
U 5D817847
F0 "LCD Displlay" 50
F1 "lcd_display.sch" 50
$EndSheet
Text Notes 12650 2050 0    50   ~ 0
Speaker and AudioConnector as Sound Output,\nMicrophone as Sound Input,\n(MicroCamera as Image/Video Input)\n
Text Notes 1350 4950 0    50   ~ 0
Allowable Error of all Resistors are less than 1%.
$EndSCHEMATC
