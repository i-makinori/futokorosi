EESchema Schematic File Version 4
LIBS:futokoroshi-cache
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 2 4
Title "Futokoroshi Pocket Computer - Power Circuit"
Date "2019-09-16"
Rev "endless-beta"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery_Cell BT?
U 1 1 5D7FE924
P 5300 3500
AR Path="/5D7FE924" Ref="BT?"  Part="1" 
AR Path="/5D7F6233/5D7FE924" Ref="BT1"  Part="1" 
F 0 "BT1" H 5418 3596 50  0000 L CNN
F 1 "Battery_Cell" H 5418 3505 50  0000 L CNN
F 2 "" V 5300 3560 50  0001 C CNN
F 3 "~" V 5300 3560 50  0001 C CNN
	1    5300 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:Thermistor TH?
U 1 1 5D7FE92A
P 5000 3450
AR Path="/5D7FE92A" Ref="TH?"  Part="1" 
AR Path="/5D7F6233/5D7FE92A" Ref="TH1"  Part="1" 
F 0 "TH1" H 5105 3496 50  0000 L CNN
F 1 "Thermistor" H 5105 3405 50  0000 L CNN
F 2 "" H 5000 3450 50  0001 C CNN
F 3 "~" H 5000 3450 50  0001 C CNN
	1    5000 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D7FE931
P 1650 2250
AR Path="/5D7FE931" Ref="C?"  Part="1" 
AR Path="/5D7F6233/5D7FE931" Ref="C1"  Part="1" 
F 0 "C1" H 1765 2296 50  0000 L CNN
F 1 "C" H 1765 2205 50  0000 L CNN
F 2 "" H 1688 2100 50  0001 C CNN
F 3 "~" H 1650 2250 50  0001 C CNN
	1    1650 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3000 4050 3000
Wire Wire Line
	1650 2400 1650 3000
Wire Wire Line
	4050 3000 4050 2800
$Comp
L Battery_Management:BQ24072RGT U?
U 1 1 5D7FE91D
P 4050 2200
AR Path="/5D7FE91D" Ref="U?"  Part="1" 
AR Path="/5D7F6233/5D7FE91D" Ref="U1"  Part="1" 
F 0 "U1" H 4050 2981 50  0000 C CNN
F 1 "BQ24072RGT" H 4050 2890 50  0000 C CNN
F 2 "Package_DFN_QFN:VQFN-16-1EP_3x3mm_P0.5mm_EP1.6x1.6mm" H 4350 1650 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/bq24072.pdf" H 4350 2400 50  0001 C CNN
	1    4050 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 1300 4050 1600
Wire Wire Line
	1650 1300 4050 1300
Wire Wire Line
	1650 1300 1650 2100
$EndSCHEMATC
