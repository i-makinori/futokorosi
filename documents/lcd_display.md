

### 現在の利用部材

- LCDディスプレイ [https://www.digikey.jp/product-detail/ja/matrix-orbital/MOP-TFT800480-50A-BLM-TPR/635-1160-ND/)
- LEDドライバ [LM3410XSD/NOPB](https://www.digikey.jp/product-detail/ja/texas-instruments/LM3410XSD-NOPB/LM3410XSD-NOPBCT-ND/1778509)
- FFC/FPC コネクタ [https://www.digikey.jp/product-detail/ja/molex/0541324062/WM3436CT-ND/2405614]


### ピン割当て

40pin 24bit パラレルインターフェイス版 LCD液晶ディスプレイ

| PIN   | SYMBOL    | Description                             | Comment |
| --    | --        | --                                      | --      |
| 1     | LED- , CA | Cathod of LED Backlight                 |         |
| 2     | LED+ , AN | Anode of LED Backlight                  |         |
| 3     | GND       | Ground                                  |         |
| 4     | VDD       | Power Supply                            |         |
| 5~12  | [R0~R7]   | Red data input R0(pin5) tp R7(pin12)    |         |
| 13~20 | [G0~G7]   | Green data input G0(pin13) to G7(pin20) |         |
| 21~28 | [B0~B7]   | Blue data input B0(pin21) to B7(pin28)  |         |
| 29    | GND       | Ground                                  |         |
| 30    | DCLK      | Dot Clock Sinnal Input (Desc1)          |         |
| 31    | DISP      | Display on/off (Desc2)                  |         |
| 32    | HS        | Horizontal sync input                   |         |
| 33    | VS        | Vertical sync input                     |         |
| 34    | DE        | Data Enable Input (desc3)               |         |
| 35    | NC        | Not Connected                           |         |
| 36    | GND       | Ground                                  |         |
| 37    | XR        | Touch panel X ringht pin (Note1)        |         |
| 38    | YD        | Touch panel Y down pin   (Note1)        |         |
| 39    | XL        | Touch panel X left pin (Note1)          |         |
| 40    | YU        | Touch panel Y up pin (Note1)            |         |

タッチパネル(例) (Note2)
| PIN | SYMBOL | Descriptioin     | Comment |
| --  | --     | --               | --      |
| 1   | xR     | x-Right terminal |         |
| 2   | yL     | y-Lower terminal |         |
| 3   | xL     | x-Left terminal  |         |
| 4   | yU     | y-Upper terminal |         |


- Note1 : if touch panel pin is united in 40pin ribon . if not, Not Connected.
- Note2 : if pin of touch panel is sepalated.
- Desc1 : Clock for input data. Data latched at rising/falling edge of this signal. Default is falling edge
- Desc2 : Standby mode control.(Normally pull high)STBYB=”L”, enter standby mode for power saving. Timing controller source driver will turn off, all outputs are Hi-Z.STBYB=”H”, normal operation.
- Desc3 : Input data enable control. When DE mode, active High to enable data input(Normally pull low)


### バックライト
16.8vが適正として､最大が19.8v.今回は18.8vの出力を決定し､部分への電流は40mA｡
詳しくは計算やデータシートなどを参照｡


### タッチパネル

xU xR yD yL の4ピンにて観測を行う､抵抗皮膜式のtouch panelでlinuxのDriverを探しているんだよね｡
未実装に付き実装の見通しも暗く調査中｡

- 参考にはならないかも知れないが [Resistive Touchscreen Controller with Espruino](https://www.espruino.com/TouchRD)

### 参考

表示方法
- [Let’s add a dirt cheap screen to the Raspberry Pi B+](http://blog.reasonablycorrect.com/raw-dpi-raspberry-pi/)
  - (材料)
  - [5.0" 40-pin TFT Display - 800x480 with Touchscreen](https://www.adafruit.com/product/1596)
  - [40-pin TFT Friend - FPC Breakout with LED Backlight Driver](https://www.adafruit.com/product/1932)
  - [Premium Female/Female Jumper Wires - 40 x 6"](https://www.adafruit.com/product/266)
- [dpi666](https://github.com/robertely/dpi666)

作品例
- [CM3-PANEL](https://www.acmesystems.it/CM3-PANEL)
  - [Schematic](https://www.acmesystems.it/www/CM3-PANEL/cm3panel_schematic.pdf)
- [Retro-CM3: Handled GAME Consol](https://www.instructables.com/id/Retro-CM3-a-Powerful-RetroPie-Handled-GAME-Console/) (Refer Step 3, it uses 45pins LCD display)


その他の液晶製品
- 4dlcd - 5.0inch TFT Liquid Crystal Display  [4DLCD-50800480-(RTP/CT)-(CLB)](https://www.4dlcd.com/specs/4DLCD-50800480-Spec-V1.0-update%202018-07-10.pdf)
- kyocera - 4.3inch WQVGA TFT with LED backlight / and touch panel.  [TCG043WQLBAAFA-GA50](http://www.kyocera-display.com/SiteImages/PartList/SPEC/TCG043WQLBAAFA-GA50Eng%20.pdf)
- [CH430WQ01-T](https://cdn-shop.adafruit.com/product-files/1591/SPEC-CH430WQ01-T_Rev.A.pdf)

