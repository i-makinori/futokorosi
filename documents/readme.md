
## Links

### Raspberry pi

- [Raspberry Pi Compute Module* Data Sheet](https://www.raspberrypi.org/documentation/hardware/computemodule/datasheets/rpi_DATA_CM_1p0.pdf)
- [Raspberry Pi hardware](https://www.raspberrypi.org/documentation/hardware/raspberrypi/README.md)

回路図の一部

- [Raspberry pi Compute Module 3 - Schematics](https://www.raspberrypi.org/documentation/hardware/computemodule/schematics/rpi_SCH_CM3_1p0.pdf)
- [Raspberry pi Compute Module IO Board - Schematics](https://www.raspberrypi.org/documentation/hardware/computemodule/schematics/rpi_SCH_CMIO_3p0.pdf)
- [Schematics](https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/README.md)

ドライバに相当するプログラム｡

- [config.txt](https://www.raspberrypi.org/documentation/configuration/config-txt/)
- [C library for Broadcom BCM 2835 as used in Raspberry Pi](http://www.airspayce.com/mikem/bcm2835/)


### 製作例

偉大なる先人たち

- [CM3-PANEL](https://www.acmesystems.it/CM3-PANEL)
- [Retro-CM3: a Powerful RetroPie Handled GAME Console](https://www.instructables.com/id/Retro-CM3-a-Powerful-RetroPie-Handled-GAME-Console/)
- [Raspberry Pi Compute Module 3 用キャリアボード](http://blog.ratocsystems.com/wp/2017/05/15/15/32/54)
- [Design Your Own Raspberry Pi Compute Module PCB](https://www.instructables.com/id/Design-Your-Own-Raspberry-Pi-Compute-Module-PCB/)

- [Pi UMPC - ウルトラモバイル PC](http://raspi10.blog.fc2.com/blog-entry-1145.html)
- [Raspberry UMPC by Pole_ergo. Jul 12, 2017](https://www.thingiverse.com/thing:2430252)
- [Nano Pi2 UMPC by Pole_ergo. Sep 4, 2017](https://www.thingiverse.com/thing:2514014)
- [This handheld Linux PC is actually a Raspberry Pi with iPhone keyboard](https://thenextweb.com/shareables/2017/03/24/handheld-linux-pc-raspberry-pi-iphone/)

その他小型コンピュータ

- [Pyra](https://www.dragonbox.de/en/pyra)
- [A list of handheld/pocket Linux computers](https://www.reddit.com/r/linux/comments/4biamr/a_list_of_handheldpocket_linux_computers/)


### 電源設計

### バッテリーの入手

- [3.7Ｖリチウムポリマー電池](https://www.nexcell.co.jp/docs/Products/polymer/polymer.htm)
- [リチウムポリマー電池](https://www.nexcell.co.jp/category/55/)
- [プロショップのリチウムポリマーバッテリー](http://www.inedenki.co.jp/category/lithium/Lipobattery.html) バルク品(要問い合わせ(?))
- [LiPo電池 3.7V 3000mAh](https://strawberry-linux.com/catalog/items?code=15094)
- [二次電池](https://biz.maxell.com/ja/rechargeable_batteries/) リンク先の特性図が参考になる

#### バッテリー制御ICについて

電源を外部のACアダプター(又はUSB給電)と､内部のリチウムバッテリーとする｡  
外部からの給電時はシステムへの給電を優先しつつバッテリーにも充電をし､外部からの給電の無き時は即座にバッテリーからの給電をしたい｡  
この要望は､Texas Instruments Incorporated (TI)の公表する､動的パワー･パス管理(DPPS)と合致する｡  

DPPS 機能を持つ集積回路
- [bq24070, bq24071](http://www.ti.com/jp/lit/ds/symlink/bq24071.pdf)
- [bqTINY (bq24030,bq24031,bq24032A,bq24035,bq24038)](http://www.ti.com/lit/ds/symlink/bq24032a.pdf)
- [バッテリー ベビー シッター](https://github.com/sparkfun/Battery_Babysitter) 1Cell Li-poバッテリーの管理のための全てがここに｡
  - [switch-science](https://www.switch-science.com/catalog/2824/)


参考程度に､DPPSに相当する機能を持ちうる電源IC
- [LTC3558 LiPo充電+昇降圧DC-DCコンバータモジュール(3.3V・1.8V)](https://strawberry-linux.com/catalog/items?code=13558) 電圧や耐電流が低い気がするが､この派生には何らかの使えるものがあるかもしれない｡
- 他にいくつかは有るかもしれない｡


その他参考｡

- [これからの電源回路設計手法](http://www.tij.co.jp/jp/lit/ml/jajb012/jajb012.pdf)
- [バスパワーとセルフパワーの自動切り替え](http://www.picfun.com/usb20/usb2003.html)
- [バッテリバックアップ回路について](https://sites.google.com/site/keistudiosite/home/%E4%B8%80%E4%BD%93%E5%9E%8Bps4%E3%82%92%E5%88%B6%E4%BD%9Cplaystation4unify/%E3%83%90%E3%83%83%E3%83%86%E3%83%AA%E3%83%90%E3%83%83%E3%82%AF%E3%82%A2%E3%83%83%E3%83%97%E5%9B%9E%E8%B7%AF%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6) 外部電源と内部電源の切り替え

#### 電圧変換

3.3v, 1.8v, 5.0v ... そして､LCDの12~30くらい｡ レギュレータが何個必要なるやら｡


### 人に対しての影像の表示, タッチパネル

- [lcd_display.md](./lcd_display.md) を参照


### キーボード

完全自作の場合
- [Tanuki 40% keyboard](https://github.com/SethSenpai/Tanuki)
- [モビ・エレクトロニック](https://pikstagram.org/mobi.electronik)
- [PDA自作日記](http://pda.pa-i.org/index.php/pda/1250.html)
- [i2c keyboard](https://sudomod.com/forum/viewtopic.php?t=8345)
- [Noodle Pi](http://www.noodlepi.com/store)


### カメラ､マイク､スピーカー

ビデオ電話をしたい

#### カメラ

#### マイク

#### スピーカー

- [Audio configuration](https://www.raspberrypi.org/documentation/configuration/audio-config.md)
- [Adding Audio Output To The Raspberry Pi Zero - Tinkernut Workbench](https://www.youtube.com/watch?v=3pXB90IDNoY)
- [CM3 - What pins are used Left/Right audio on 5pin jack?](https://www.raspberrypi.org/forums/viewtopic.php?t=197267)


### システム外部との交信

#### 位置情報システム GNSS

地図を使いたい｡

#### 情報交信則 (Internet Protocol)

Ethernetも持たせようと思っていたが､結局はWifiだけになる気がする


### その他の拡張端子

- ACアダプターのメス端子 (micro USBになるか?)
- USB端子 * 1~2 くらい
- HDMI
- 音声端子
- SD Card, Micro SD Card (?)



## 妄想

FLEPia か､､､電子ペーパそれもフルカラーでそれなりの応答速度か､､､｡ 欲しい｡